import React,{Component} from 'react';
import './UserOutput.css';

class UserOutput extends Component{

    render(){
        return (
            <div className="UserOutput">
                <p>THIS IS THE INPUT {this.props.name}</p>
                <p>THI IS THE TWO WAY BINDED ONE {this.props.description}</p>
            </div>
        );
    }
}

export default UserOutput;