import React,{Component} from 'react';
import logo from './logo.svg';
import './App.css';
import UserInput from './UserInput.js';
import UserOutput from './UserOutput.js';

class App extends Component {

  

  state={
    'UserInput':'Default User Input'
  }

  stateChangeHandler = (event) =>{
    this.setState({
      'UserInput':event.target.value
    });
  }

  render(){
    const style={
      backgroundColor:'yellow',
      font: 'inherit',
      Color:'red'
    };
    return (
      <div className="App">
        <UserOutput name="First" description={this.state.UserInput}/>
        <UserOutput name="Second" description={this.state.UserInput}/>
        <UserOutput name="Third" description={this.state.UserInput}/>
        <UserInput change={this.stateChangeHandler} default={this.state.UserInput} style={style}/>
      </div>
    );
  }
  
}

export default App;
