import React,{Component} from 'react';


class UserInput extends Component{

    render(){
        return (
            <div>
                <input type="text" onChange={this.props.change} value={this.props.default}/>
            </div>
        );
    }
}

export default UserInput